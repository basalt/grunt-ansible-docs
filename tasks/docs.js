/**
 * Module with the grunt-ansible-docs
 *
 * @module grunt-ansible-docs
 * @author Basalt AB
 * @license
 * Copyright (c) 2018 Basalt AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

module.exports = function(grunt) {

  grunt.registerTask('ansible-docs', 'Generate Ansible documentation.', function(arg1) {
    var AnsibleParser = require('./lib/parser');
    var parser;
    var targets = [];
    var target;

    if (typeof(arg1) === 'undefined' || arg1 == 'all') {
      for (var k of Object.keys(grunt.config.data['ansible-docs'])) {
        if (k != 'config') {
          targets.push(k);
        }
      }
    }

    if (targets == []) { targets = ['all']; }

    for (target of targets) {
      parser = new AnsibleParser();
      var opts = this.options({
        inventory: 'inventory',
        roles: 'roles',
        group_vars: 'group_vars',
        dest: 'ansible_docs.json'
      });

      if (target in grunt.config.data['ansible-docs']) {
        opts = Object.assign(opts, grunt.config.data['ansible-docs'][target]);
      }

      if (typeof opts.inventory !== undefined) {
        grunt.log.writeln("Parsing inventory at '" + opts.inventory + "'");
        parser.parseInventory(opts.inventory);
      }

      if (typeof opts.roles !== undefined) {
        grunt.log.writeln("Parsing role vars at '" + opts.roles + "'");
        parser.parseRoles(opts.roles);
      }

      if (typeof opts.group_vars !== undefined) {
        grunt.log.writeln("Parsing group vars at '" + opts.group_vars + "'");
        parser.parseGroupVars(opts.group_vars);
      }

      grunt.file.write(opts.dest, JSON.stringify(parser.docs));
      grunt.log.ok("Saved documentation at '" + opts.dest + "'");
    }
  });
};
