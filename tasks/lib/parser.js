/**
 * Module with the AnsibleParser for parsing Ansible documentation
 *
 * @module AnsibleParser
 * @author Basalt AB
 * @license
 * Copyright (c) 2018 Basalt AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

var Path = require('path');
var Fs = require('fs');
var Glob = require("glob");
var YAML = require('yaml');

/**
 * Represents a parser of Ansible documentation
 */
class AnsibleParser {

  constructor() {
    this.docs = {
      "vars": {},
      "inventory": {}
    };
  }

  /**
   * Parse an inventory file
   *
   * @param {string} path Path to the inventory file (ini format)
   */
  parseInventory(path) {
    this.docs.inventory = this.parseIniFile(path);
  }

  /**
   * Parse group vars
   *
   * @param {string} path Folder where group vars are located
   */
  parseGroupVars(path) {
    var glob = Path.join(path, '**', '*.yml');
    var vars = {};
    var that = this;

    Glob.sync(glob).forEach(function(file) {
      vars = that.parseVarFile(file, vars);
      for (const [key, value] of Object.entries(vars)) {
        that.docs.vars[key] = Object.assign(that.docs.vars[key] || {}, value);
      }
    });
  };

  /**
   * Parse default vars for roles
   *
   * @param {string} path Folder where roles are located
   */
  parseRoles(path) {
    var glob = Path.join(path, '**', 'defaults', '**', '*.yml');
    var vars = {};
    var that = this;

    Glob.sync(glob).forEach(function(file) {
      vars = that.parseVarFile(file, vars);
      for (const [key, value] of Object.entries(vars)) {
        that.docs.vars[key] = Object.assign(that.docs.vars[key] || {}, value);
      }
    });
  };

  // @private
  parseIniFile(filepath) {
    var result = {};
    var commentBuffer = [];
    var valueBuffer = [];
    var group = '';
    var previousGroup = '';

    var commentPattern = /(^\s*#$|^\s*# (.*))/;
    var groupPattern = /\[(.*)\]/;

    if (!Fs.existsSync(filepath)) {
      return result;
    }

    var file = Fs.readFileSync(filepath).toString();
    file.split("\n").forEach(function(line) {

      // line is a group
      if (groupPattern.test(line)) {

        // save value of previous group
        if (group != '') {
          result[group]['value'] = valueBuffer.join("\n");
        }

        // save description of this group
        group = line.match(groupPattern)[1];
        result[group] = {
          "description": commentBuffer.join("\n")
        };

        // reset buffers
        valueBuffer = [];
        commentBuffer = [];
      }

      // line is a comment
      else if (commentPattern.test(line)) {
        commentBuffer.push(line.match(commentPattern)[2]);
      }

      // line is value
      else {
        valueBuffer.push(line.trim());
      }
    });

    // save value of last group
    if (group != '') {
      result[group]['value'] = valueBuffer.join("\n");
    }

    return result;
  }

  // @private
  parseVarFile(filepath, result) {
    var buffer = [];
    var name = '';
    var description = '';
    var yaml;

    var commentPattern = /(^\s*#$|^\s*# (.*))/;
    var varPattern = /^(\w[\w-]*):/;

    var file = Fs.readFileSync(filepath, 'utf8').toString();
    var docs = YAML.parseAllDocuments(file);
    file.split("\n").forEach(function(line) {

      // line is a comment
      if (commentPattern.test(line)) {
        buffer.push(line.match(commentPattern)[2]);
      }

      // line is a var
      else if (varPattern.test(line)) {
        name = line.match(varPattern)[1];
        description = buffer.join("\n").trim();

        // initialize var object
        if (typeof(result[name]) === 'undefined') {
          result[name] = {};
        }

        // save default value
        for (var doc in docs) {
          yaml = docs[doc].toJSON();
          if (yaml && name in yaml) {
            result[name]['value'] = YAML.stringify(yaml[name]).trim();
          }
        }

        // save description
        if (description != '') {
          result[name]['description'] = description;
        }

        buffer = [];
      }

      else {
        buffer = [];
      }
    });

    return result;
  }
}

module.exports = AnsibleParser;
