'use strict';

var Fs = require('fs');
var Glob = require('glob');

describe("AnsibleParser", function() {
  var parser;
  var AnsibleParser = require('../tasks/lib/parser.js');

  beforeEach(function() {
    parser = new AnsibleParser();
  });

  describe(".parseInventory", function() {

    it("should parse inventory file", function() {
      spyOn(Fs, 'existsSync').withArgs('test').and.returnValue(true);
      spyOn(Fs, 'readFileSync').withArgs('test').and.returnValue(`
        # comment for group1
        [group1]
        server1
        [group2]
        server2`);
      parser.parseInventory('test');
      expect(parser.docs).toBeDefined();
      expect(parser.docs.inventory).toBeDefined();
      expect(parser.docs.inventory.group1.value).toEqual('server1');
      expect(parser.docs.inventory.group1.description)
        .toEqual('comment for group1');
      expect(parser.docs.inventory.group2.value).toEqual('server2');
      expect(parser.docs.inventory.group2.description)
        .toEqual('');
    });

    it("should parse empty inventory", function() {
      spyOn(Fs, 'existsSync').withArgs('test').and.returnValue(true);
      spyOn(Fs, 'readFileSync').withArgs('test').and.returnValue('');
      parser.parseInventory('test');
      expect(parser.docs).toBeDefined();
      expect(parser.docs.inventory).toBeDefined();
      expect(parser.docs.inventory).toEqual({});
    });

    it("should skip non-existent inventory", function() {
      spyOn(Fs, 'existsSync').withArgs('test').and.returnValue(false);
      parser.parseInventory('test');
      expect(parser.docs).toBeDefined();
      expect(parser.docs.inventory).toBeDefined();
      expect(parser.docs.inventory).toEqual({});
    });

  });

  describe(".parseGroupVars", function() {

    it("should parse group_vars", function() {

      var mockFiles = {
        'group_vars/a.yml': `# comment for var_x
var_x: value_x_a
# comment for var_y from a
var_y: value_y_a
var_z: value_z_a`,

        'group_vars/b.yml': `---
var_x: value_x_b
# comment for var_y from b
var_y: value_y_b
...
# comment for var_z
#
# on multiple lines
var_z: value_z_b`
      };

      spyOn(Glob, 'sync').withArgs('group_vars/**/*.yml').and
        .returnValue(Object.keys(mockFiles));
      spyOn(Fs, 'readFileSync').and.callFake(function() {
        return mockFiles[arguments[0]];
      });

      parser.parseGroupVars('group_vars');
      expect(parser.docs).toBeDefined();
      expect(parser.docs.vars).toBeDefined();
      expect(parser.docs.vars.var_x.value).toEqual('value_x_b');
      expect(parser.docs.vars.var_x.description)
        .toEqual('comment for var_x');
      expect(parser.docs.vars.var_y.value).toEqual('value_y_b');
      expect(parser.docs.vars.var_y.description)
        .toEqual('comment for var_y from b');
      expect(parser.docs.vars.var_z.value).toEqual('value_z_b');
      expect(parser.docs.vars.var_z.description)
        .toEqual("comment for var_z\n\non multiple lines");
    });

    it("should not remove description when empty", function() {

      var mockFiles = {
        'roles/role_a/defaults/main.yml': `# comment for var_x\nvar_x: value_role`,
        'group_vars/a.yml': `var_x: value_group`
      };

      var mockRoleFiles = {
        'roles/role_a/defaults/main.yml': mockFiles['roles/role_a/defaults/main.yml']
      };

      var mockGroupFiles = {
        'group_vars/a.yml': mockFiles['group_vars/a.yml']
      };

      spyOn(Fs, 'readFileSync').and.callFake(function() {
        return mockFiles[arguments[0]];
      });

      spyOn(Glob, 'sync')
        .withArgs('roles/**/defaults/**/*.yml').and.returnValue(Object.keys(mockRoleFiles))
        .withArgs('group_vars/**/*.yml').and.returnValue(Object.keys(mockGroupFiles));

      parser.parseRoles('roles');
      parser.parseGroupVars('group_vars');

      expect(parser.docs).toBeDefined();
      expect(parser.docs.vars).toBeDefined();
      expect(parser.docs.vars.var_x.value).toEqual('value_group');
      expect(parser.docs.vars.var_x.description)
        .toEqual('comment for var_x');
    });

  });

  describe("parseRoles", function() {

    it("should parse role vars", function() {

      var mockFiles = {
        'roles/role_a/defaults/main.yml': `# comment for var_x
var_x: value_x_a
# comment for var_y from a
var_y: value_y_a
var_z: value_z_a`,

        'roles/role_b/defaults/main.yml': `# comment for var_y from b
var_y: value_y_b
# comment for var_z
#
# on multiple lines
var_z: value_z_b`
      };

      spyOn(Glob, 'sync').withArgs('roles/**/defaults/**/*.yml').and
        .returnValue(Object.keys(mockFiles));
      spyOn(Fs, 'readFileSync').and.callFake(function() {
        return mockFiles[arguments[0]];
      });

      parser.parseRoles('roles');
      expect(parser.docs).toBeDefined();
      expect(parser.docs.vars).toBeDefined();
      expect(parser.docs.vars.var_x.value).toEqual('value_x_a');
      expect(parser.docs.vars.var_x.description)
        .toEqual('comment for var_x');
      expect(parser.docs.vars.var_y.value).toEqual('value_y_b');
      expect(parser.docs.vars.var_y.description)
        .toEqual('comment for var_y from b');
      expect(parser.docs.vars.var_z.value).toEqual('value_z_b');
      expect(parser.docs.vars.var_z.description)
        .toEqual("comment for var_z\n\non multiple lines");
    });

  });
});
